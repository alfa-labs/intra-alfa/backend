﻿using IntraAlfa.Domain.Entities;

namespace IntraAlfa.Infra.Interfaces
{
    public interface IPurchaseOrderRepository : IBaseRepository<PurchaseOrder>
    {

    }
}
