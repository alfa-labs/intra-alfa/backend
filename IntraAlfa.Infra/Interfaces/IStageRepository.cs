using IntraAlfa.Domain.Entities;

namespace IntraAlfa.Infra.Interfaces
{
    public interface IStageRepository : IBaseRepository<Stage>
    {
    }
}