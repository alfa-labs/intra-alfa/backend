using IntraAlfa.Domain.Entities;

namespace IntraAlfa.Infra.Interfaces
{
    public interface IUserRepository : IBaseRepository<User>
    {
    }
}