using IntraAlfa.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IntraAlfa.Infra.Mappings
{
    public class StageMap : IEntityTypeConfiguration<Stage>
    {
        public void Configure(EntityTypeBuilder<Stage> builder)
        {
            builder.ToTable("Stage");

            builder.HasKey(x => x.Id);

            builder.Property(x=>x.Id)
                .UseMySqlIdentityColumn()
                .HasColumnType("BIGINT");

            builder.Property(x => x.EntityId)
                .IsRequired()
                .HasColumnName("entityId")
                .HasColumnType("BIGINT");

            builder.Property(x => x.DocumentType)
                .IsRequired()
                .HasMaxLength(4)
                .HasColumnName("documentType")
                .HasColumnType("VARCHAR(4)");

            builder.Property(x => x.ApproverEmail)
                .IsRequired()
                .HasMaxLength(200)
                .HasColumnName("approverEmail")
                .HasColumnType("VARCHAR(200)");

            builder.Property(x => x.Remarks)
                .IsRequired(false)
                .HasMaxLength(200)
                .HasColumnName("remarks")
                .HasColumnType("VARCHAR(200)");


            builder.Property(x => x.Status)
                .IsRequired()
                .HasMaxLength(1)
                .HasColumnName("status")
                .HasColumnType("VARCHAR(1)");

            builder.Property(x => x.ApproveAt)
                .IsRequired(false)
                .HasMaxLength(1)
                .HasColumnName("approveAt")
                .HasColumnType("TimeStamp");
        }
    }
}