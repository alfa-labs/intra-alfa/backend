﻿using IntraAlfa.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IntraAlfa.Infra.Mappings
{
    public class PurchaseOrderMap : IEntityTypeConfiguration<PurchaseOrder>
    {
        public void Configure(EntityTypeBuilder<PurchaseOrder> builder)
        {
            builder.ToTable("PurchaseOrder");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id)
                .UseMySqlIdentityColumn()
                .HasColumnType("BIGINT");

            builder.Property(x => x.CreateDate)
                .IsRequired()
                .HasColumnName("createDate")
                .HasColumnType("DateTime");

            builder.Property(x => x.UpdateDate)
                .IsRequired()
                .HasColumnName("updateDate")
                .HasColumnType("DateTime");

            builder.Property(x => x.Remarks)
                .HasMaxLength(180)
                .HasColumnName("remarks")
                .HasColumnType("VARCHAR(1000)");

            builder.Property(x => x.TotalPrice)
                .IsRequired()
                .HasMaxLength(1)
                .HasColumnName("totalPrice")
                .HasColumnType("DECIMAL(7, 2)");

            builder.Property(x => x.Status)
                .IsRequired()
                .HasMaxLength(1)
                .HasColumnName("status")
                .HasColumnType("VARCHAR(1)");

            builder.Property(x => x.Provider)
                .IsRequired()
                .HasMaxLength(180)
                .HasColumnName("provider")
                .HasColumnType("VARCHAR(180)");

            builder.Property(x => x.Approver)
                .IsRequired(false)
                .HasMaxLength(100)
                .HasColumnName("approver")
                .HasColumnType("VARCHAR(100)");

            builder.Property(x => x.ApproveDate)
                .IsRequired(false)
                .HasColumnName("approveDate")
                .HasColumnType("DATETIME");

            builder.Property(x => x.FiscalCodeProvider)
                .IsRequired(false)
                .HasMaxLength(14)
                .HasColumnName("fiscalCodeProvider")
                .HasColumnType("VARCHAR(14)");

            builder.Property(x => x.AddressProvider)
                .IsRequired(false)
                .HasMaxLength(200)
                .HasColumnName("addressProvider")
                .HasColumnType("VARCHAR(200)");

            builder.Property(x => x.MailProvider)
                .IsRequired(false)
                .HasMaxLength(150)
                .HasColumnName("mailProvider")
                .HasColumnType("VARCHAR(150)");

            builder.Property(x => x.FinancialRemarks)
                .IsRequired(false)
                .HasMaxLength(200)
                .HasColumnName("financialRemarks")
                .HasColumnType("VARCHAR(200)");

            builder.Property(x => x.ItemDescription)
                .IsRequired(false)
                .HasMaxLength(2000)
                .HasColumnName("itemDescription")
                .HasColumnType("VARCHAR(2000)");

            builder.Property(x => x.PaymentType)
                .IsRequired(false)
                .HasMaxLength(30)
                .HasColumnName("paymentType")
                .HasColumnType("VARCHAR(30)");

            builder.Property(x => x.ProviderCodeBank)
                .IsRequired(false)
                .HasMaxLength(3)
                .HasColumnName("providerCodeBank")
                .HasColumnType("VARCHAR(3)");

            builder.Property(x => x.ProviderAgBank)
                .IsRequired(false)
                .HasMaxLength(7)
                .HasColumnName("providerAgBank")
                .HasColumnType("VARCHAR(7)");

            builder.Property(x => x.ProviderAccountBank)
                .IsRequired(false)
                .HasMaxLength(20)
                .HasColumnName("providerAccountBank")
                .HasColumnType("VARCHAR(20)");

            builder.Property(x => x.Requirer)
                .IsRequired()
                .HasMaxLength(100)
                .HasColumnName("requirer")
                .HasColumnType("VARCHAR(100)");

            builder.Property(x => x.InitialPeriod)
                .IsRequired(false)
                .HasColumnName("initialPeriod")
                .HasColumnType("DATETIME");

            builder.Property(x => x.FinalPeriod)
                .IsRequired(false)
                .HasColumnName("finalPeriod")
                .HasColumnType("DATETIME");

            builder.Property(x => x.AutRenew)
                .IsRequired(false)
                .HasColumnName("AutRenew")
                .HasColumnType("BIT");
        }
    }
}
