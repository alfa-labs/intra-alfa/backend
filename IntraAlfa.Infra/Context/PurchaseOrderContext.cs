﻿using IntraAlfa.Domain.Entities;
using IntraAlfa.Infra.Mappings;
using Microsoft.EntityFrameworkCore;

namespace IntraAlfa.Infra.Context
{
    public class PurchaseOrderContext : DbContext
    {
        public PurchaseOrderContext()
        { }

        public PurchaseOrderContext(DbContextOptions<PurchaseOrderContext> options) : base(options)
        { }

        public virtual DbSet<PurchaseOrder> PurchaseOrders { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new PurchaseOrderMap());
        }
    }
}
