using IntraAlfa.Domain.Entities;
using IntraAlfa.Infra.Mappings;
using Microsoft.EntityFrameworkCore;

namespace IntraAlfa.Infra.Context
{
    public class UserContext : DbContext
    {
        public UserContext()
        { }

        public UserContext(DbContextOptions<UserContext> options) : base(options)
        { }

        public virtual DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new UserMap());
        }
    }
}