using IntraAlfa.Domain.Entities;
using IntraAlfa.Infra.Mappings;
using Microsoft.EntityFrameworkCore;

namespace IntraAlfa.Infra.Context
{
    public class StageContext : DbContext
    {
        public StageContext()
        { }

        public StageContext(DbContextOptions<StageContext> options) : base(options)
        { }

        public virtual DbSet<Stage> Stages { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new StageMap());
        }
    }
}