using IntraAlfa.Domain.Entities;
using IntraAlfa.Infra.Context;
using IntraAlfa.Infra.Interfaces;

namespace IntraAlfa.Infra.Repositories
{
    public class UserRepository : BaseUserRepository<User>, IUserRepository{
        private readonly UserContext _context;

        public UserRepository(UserContext context) : base(context)
        {
            _context = context;
        }
    }
}