using IntraAlfa.Domain.Entities;
using IntraAlfa.Infra.Context;
using IntraAlfa.Infra.Interfaces;

namespace IntraAlfa.Infra.Repositories
{
    public class StageRepository : BaseStageRepository<Stage>, IStageRepository{
        private readonly StageContext _context;

        public StageRepository(StageContext context) : base(context)
        {
            _context = context;
        }
    }
}