﻿using IntraAlfa.Domain.Entities;
using IntraAlfa.Infra.Context;
using IntraAlfa.Infra.Interfaces;

namespace IntraAlfa.Infra.Repositories
{
    public class PurchaseOrderRepository : BasePurchaseOrderRepository<PurchaseOrder>, IPurchaseOrderRepository
    {
        private readonly PurchaseOrderContext _context;

        public PurchaseOrderRepository(PurchaseOrderContext context) : base(context)
        {
            _context = context;
        }
    }
}
