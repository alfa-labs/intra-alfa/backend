using System.Linq.Expressions;
using AutoMapper;
using EscNet.Cryptography.Interfaces;
using IntraAlfa.Core.Communication.Mediator.Interfaces;
using IntraAlfa.Core.Communication.Messages.Notifications;
using IntraAlfa.Core.Enum;
using IntraAlfa.Core.Structs;
using IntraAlfa.Core.Validations.Message;
using IntraAlfa.Domain.Entities;
using IntraAlfa.Infra.Interfaces;
using IntraAlfa.Services.Dto;
using IntraAlfa.Services.Interfaces;

namespace IntraAlfa.Services.Services
{
    public class UserService : IUserService
    {
        private readonly IMapper _mapper;
        private readonly IUserRepository _userRepository;
        private readonly IRijndaelCryptography _rijndaelCryptography;
        private readonly IMediatorHandler _mediator;

        public UserService(
            IMapper mapper,
            IUserRepository userRepository,
            IRijndaelCryptography rijndaelCryptography,
            IMediatorHandler mediator)
        {
            _mapper = mapper;
            _userRepository = userRepository;
            _rijndaelCryptography = rijndaelCryptography;
            _mediator = mediator;
        }

        public async Task<Optional<UserDTO>> CreateAsync(UserDTO userDTO)
        {
            Expression<Func<User, bool>> filter = user
                => user.Email.ToLower() == userDTO.Email.ToLower();

            var userExists = await _userRepository.GetAsync(filter);

            if (userExists != null)
            {
                await _mediator.PublishDomainNotificationAsync(new DomainNotification(
                    ErrorMessages.RecordAlreadyExists,
                    DomainNotificationType.RecordAlreadyExists));

                return new Optional<UserDTO>();
            }

            var user = _mapper.Map<User>(userDTO);
            //user.Validate();

            if (!user.IsValid)
            {
                await _mediator.PublishDomainNotificationAsync(new DomainNotification(
                   ErrorMessages.RecordInvalid(user.ErrorsToString()),
                   DomainNotificationType.RecordInvalid));

                return new Optional<UserDTO>();
            }

            user.SetPassword(_rijndaelCryptography.Encrypt(user.Password));

            var userCreated = await _userRepository.CreateAsync(user);

            return _mapper.Map<UserDTO>(userCreated);
        }

        public async Task<Optional<UserDTO>> UpdateAsync(UserDTO userDTO)
        {
            var userExists = await _userRepository.GetAsync(userDTO.Id);

            if (userExists == null)
            {
                await _mediator.PublishDomainNotificationAsync(new DomainNotification(
                   ErrorMessages.RecordNotFound,
                   DomainNotificationType.RecordNotFound));

                return new Optional<UserDTO>();
            }

            var user = _mapper.Map<User>(userDTO);
            //user.Validate();

            if (!user.IsValid)
            {
                await _mediator.PublishDomainNotificationAsync(new DomainNotification(
                   ErrorMessages.RecordInvalid(user.ErrorsToString()),
                   DomainNotificationType.RecordInvalid));

                return new Optional<UserDTO>();
            }

            user.SetPassword(_rijndaelCryptography.Encrypt(user.Password));

            var userUpdated = await _userRepository.UpdateAsync(user);

            return _mapper.Map<UserDTO>(userUpdated);
        }
        public async Task RemoveAsync(long id)
            => await _userRepository.RemoveAsync(id);

        public async Task<Optional<UserDTO>> GetAsync(long id)
        {
            var user = await _userRepository.GetAsync(id);

            return _mapper.Map<UserDTO>(user);
        }

        public async Task<Optional<IList<UserDTO>>> GetAllAsync()
        {
            var allUsers = await _userRepository.GetAllAsync();
            var allUsersDTO = _mapper.Map<IList<UserDTO>>(allUsers);

            return new Optional<IList<UserDTO>>(allUsersDTO);
        }

        public async Task<Optional<IList<UserDTO>>> SearchByNameAsync(string name)
        {
            Expression<Func<User, bool>> filter = u
                => u.Name.ToLower().Contains(name.ToLower());

            var allUsers = await _userRepository.SearchAsync(filter);
            var allUsersDTO = _mapper.Map<IList<UserDTO>>(allUsers);

            return new Optional<IList<UserDTO>>(allUsersDTO);
        }

        public async Task<Optional<IList<UserDTO>>> SearchByEmailAsync(string email)
        {
            Expression<Func<User, bool>> filter = user
                => user.Email.ToLower().Contains(email.ToLower());

            var allUsers = await _userRepository.SearchAsync(filter);
            var allUsersDTO = _mapper.Map<IList<UserDTO>>(allUsers);

            return new Optional<IList<UserDTO>>(allUsersDTO);
        }

        public async Task<Optional<UserDTO>> GetByEmailAsync(string email)
        {
            Expression<Func<User, bool>> filter = user
                => user.Email.ToLower() == email.ToLower();

            var user = await _userRepository.GetAsync(filter);
            var userDTO = _mapper.Map<UserDTO>(user);

            return new Optional<UserDTO>(userDTO);
        }

        public async Task<bool> GetByEmailAndPasswordAsync(LoginDTO loginDTO)
        {
            var psw = _rijndaelCryptography.Encrypt(loginDTO.Password);

            Expression<Func<User, bool>> filter = user
                => user.Email.ToLower() == loginDTO.Login.ToLower()
                && user.Password == psw;

            var user = await _userRepository.GetAsync(filter);

            if (user == null)
            {
                return false;
            }

            return true;

        }

        public async Task<Optional<IList<ApproverDTO>>> GetApproversAsync()
        {
            var allUsers = await _userRepository.GetAllAsync();
            var allUsersDTO = _mapper.Map<IList<ApproverDTO>>(allUsers);

            return new Optional<IList<ApproverDTO>>(allUsersDTO);
        }
    }
}