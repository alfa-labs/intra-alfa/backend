using AutoMapper;
using Coravel.Mailer.Mail.Interfaces;
using IntraAlfa.Core.Communication.Mediator.Interfaces;
using IntraAlfa.Core.Communication.Messages.Notifications;
using IntraAlfa.Core.Enum;
using IntraAlfa.Core.Structs;
using IntraAlfa.Core.Validations.Message;
using IntraAlfa.Domain.Entities;
using IntraAlfa.Infra.Interfaces;
using IntraAlfa.Services.Dto;
using IntraAlfa.Services.Interfaces;
using IntraAlfa.Services.Models;
using Microsoft.Extensions.Configuration;
using System.Linq.Expressions;

namespace IntraAlfa.Services.Services
{
    public class StageService : IStageService
    {
        private readonly IMapper _mapper;
        private readonly IStageRepository _stageRepository;
        private readonly IPurchaseOrderRepository _purchaseOrderRepository;
        private readonly IConfiguration _settings;
        private readonly IMailer _mailer;
        private readonly IMediatorHandler _mediator;

        public StageService(
            IMapper mapper,
            IStageRepository stageRepository,
            IPurchaseOrderRepository purchaseOrderRepository,
            IConfiguration settings,
            IMailer mailer,
            IMediatorHandler mediator)
        {
            _mapper = mapper;
            _mailer = mailer;
            _settings = settings;
            _stageRepository = stageRepository;
            _purchaseOrderRepository = purchaseOrderRepository;
            _mediator = mediator;
        }

        public async Task<Optional<List<StageDTO>>> CreateAsync(StagesDTO stageDTO)
        {
            try
            {
                var mailList = stageDTO.ApproverEmail;
                var result = new List<StageDTO>();

                foreach (var mail in mailList)
                {
                    var stage = _mapper.Map<Stage>(stageDTO);
                    stage.SetApproverEmail(mail);
                    stage.SetApproveAt(DateTime.Now);

                    var stageCreated = await _stageRepository.CreateAsync(stage);
                    var toDTO = _mapper.Map<StageDTO>(stageCreated);

                    result.Add(toDTO);
                }

                var purchaseOrder = await _purchaseOrderRepository.GetAsync(stageDTO.EntityId);
                purchaseOrder.SetStatus("A");

                await _purchaseOrderRepository.UpdateAsync(purchaseOrder);

                await _sendMail(result);

                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        private async Task _sendMail(List<StageDTO> mailList)
        {
            string from = _settings.GetSection("Coravel:Mail:Username").Value;
            string urlPortal = _settings.GetSection("UrlPortal").Value;

            try
            {

                foreach (var mail in mailList)
                {
                    await _mailer.SendAsync(new PortalAprovacoesMailable(
                                                                new NewUserMailableViewModel
                                                                {
                                                                    Email = mail.ApproverEmail,
                                                                    CallbackUrl = $"{urlPortal}/approval/{mail.EntityId}",
                                                                    Id = mail.EntityId,
                                                                    TypeReg = "OC"
                                                                },
                                                                from
                                                            ));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task RemoveAsync(long id)
        => await _stageRepository.RemoveAsync(id);

        public async Task<Optional<StageDTO>> GetAsync(long id)
        {
            var user = await _stageRepository.GetAsync(id);

            return _mapper.Map<StageDTO>(user);
        }

        public async Task<Optional<IList<StageDTO>>> GetAllByEmailAsync(string mail)
        {
            Expression<Func<Stage, bool>> filter = stage
                => stage.ApproverEmail.ToLower() == mail.ToLower();

            var allStages = await _stageRepository.SearchAsync(filter);
            var allStagesDTO = _mapper.Map<IList<StageDTO>>(allStages);

            return new Optional<IList<StageDTO>>(allStagesDTO);
        }

        public async Task<Optional<StageDTO>> Approve(long id, StageDTO stageDTO)
        {
            var stageExists = await _stageRepository.GetAsync(id);

            if (stageExists == null)
            {
                await _mediator.PublishDomainNotificationAsync(new DomainNotification(
                   ErrorMessages.RecordNotFound,
                   DomainNotificationType.RecordNotFound));

                return new Optional<StageDTO>();
            }

            var stage = _mapper.Map<Stage>(stageDTO);
            stage.Id = id;

            var stageUpdated = await _stageRepository.UpdateAsync(stage);

            Expression<Func<Stage, bool>> filter = stage
                => stage.EntityId == stageDTO.EntityId
                && stage.Status == "P";

            var stagePending = await _stageRepository.GetAsync(filter);

            if (stagePending == null)
            {
                var purchaseOrder = await _purchaseOrderRepository.GetAsync(stageDTO.EntityId);
                purchaseOrder.SetStatus("F");

                await _purchaseOrderRepository.UpdateAsync(purchaseOrder);
            }

            return _mapper.Map<StageDTO>(stageUpdated);
        }

        public async Task<Optional<StageDTO>> Reject(long id, StageDTO stageDTO)
        {
            var stageExists = await _stageRepository.GetAsync(id);

            if (stageExists == null)
            {
                await _mediator.PublishDomainNotificationAsync(new DomainNotification(
                   ErrorMessages.RecordNotFound,
                   DomainNotificationType.RecordNotFound));

                return new Optional<StageDTO>();
            }

            var stage = _mapper.Map<Stage>(stageDTO);
            stage.Id = id;

            var stageUpdated = await _stageRepository.UpdateAsync(stage);

            var purchaseOrder = await _purchaseOrderRepository.GetAsync(stageDTO.EntityId);
            purchaseOrder.SetStatus("R");

            await _purchaseOrderRepository.UpdateAsync(purchaseOrder);

            return _mapper.Map<StageDTO>(stageUpdated);
        }
    }
}