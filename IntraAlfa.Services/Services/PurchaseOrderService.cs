﻿using AutoMapper;
using IntraAlfa.Core.Communication.Mediator.Interfaces;
using IntraAlfa.Core.Communication.Messages.Notifications;
using IntraAlfa.Core.Enum;
using IntraAlfa.Core.Structs;
using IntraAlfa.Core.Validations.Message;
using IntraAlfa.Domain.Entities;
using IntraAlfa.Infra.Interfaces;
using IntraAlfa.Services.Dto;
using IntraAlfa.Services.Interfaces;

namespace IntraAlfa.Services.Services
{
    public class PurchaseOrderService : IPurchaseOrderService
    {
        private readonly IMapper _mapper;
        private readonly IPurchaseOrderRepository _purchaseOrderRepository;
        private readonly IMediatorHandler _mediator;

        public PurchaseOrderService(IMapper mapper,
            IPurchaseOrderRepository purchaseOrderRepository,
            IMediatorHandler mediator)
        {
            _mapper = mapper;
            _purchaseOrderRepository = purchaseOrderRepository;
            _mediator = mediator;
        }

        public async Task<Optional<PurchaseOrderDto>> CreateAsync(PurchaseOrderDto purchaseOrderDto)
        {
            var purchaseOrder = _mapper.Map<PurchaseOrder>(purchaseOrderDto);

            purchaseOrder.SetStatus("P");

            var purchaseOrderCreated = await _purchaseOrderRepository.CreateAsync(purchaseOrder);

            return _mapper.Map<PurchaseOrderDto>(purchaseOrderCreated);
        }

        public async Task<Optional<IList<PurchaseOrderDto>>> GetAllAsync()
        {
            var allPurchases = await _purchaseOrderRepository.GetAllAsync();
            var allPurchasesDTO = _mapper.Map<IList<PurchaseOrderDto>>(allPurchases);

            return new Optional<IList<PurchaseOrderDto>>(allPurchasesDTO);
        }

        public async Task<Optional<PurchaseOrderDto>> GetAsync(long id)
        {
            var purchaseOrder = await _purchaseOrderRepository.GetAsync(id);

            return _mapper.Map<PurchaseOrderDto>(purchaseOrder);
        }

        public async Task RemoveAsync(long id) => await _purchaseOrderRepository.RemoveAsync(id);

        public async Task<Optional<PurchaseOrderDto>> UpdateAsync(long id, PurchaseOrderDto purchaseOrderDTO)
        {
            var purchaseExists = await _purchaseOrderRepository.GetAsync(id);

            if (purchaseExists == null)
            {
                await _mediator.PublishDomainNotificationAsync(new DomainNotification(
                   ErrorMessages.RecordNotFound,
                   DomainNotificationType.RecordNotFound));

                return new Optional<PurchaseOrderDto>();
            }

            var purchaseOrder = _mapper.Map<PurchaseOrder>(purchaseOrderDTO);
            var purchaseUpdated = await _purchaseOrderRepository.UpdateAsync(purchaseOrder);

            return _mapper.Map<PurchaseOrderDto>(purchaseUpdated);
        }
    }
}
