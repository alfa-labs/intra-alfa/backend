﻿namespace IntraAlfa.Services.Dto
{
    public class PurchaseOrderDto
    {
        public long Id { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public DateTime ApproveDate { get; set; }
        public string Provider { get; set; }
        public string Remarks { get; set; }
        public string ItemDescription { get; set; }
        public string Status { get; set; }
        public string Approver { get; set; }
        public decimal TotalPrice { get; set; }

        public string Requirer { get; set; }
        public DateTime InitialPeriod { get; set; }
        public DateTime FinalPeriod { get; set; }
        public bool AutRenew { get; set; }
        public string FiscalCodeProvider { get; set; }
        public string AddressProvider { get; set; }
        public string MailProvider { get; set; }
        public string FinancialRemarks { get; set; }
        public string PaymentType { get; set; }
        public string ProviderCodeBank { get; set; }
        public string ProviderAgBank { get; set; }
        public string ProviderAccountBank { get; set; }


        public PurchaseOrderDto() { }

        public PurchaseOrderDto(
            long id,
            DateTime createDate,
            DateTime updateDate,
            string provider,
            string remarks,
            string status,
            string approver,
            DateTime approveDate,
            decimal totalPrice,
         string requirer,
         DateTime initialPeriod,
         DateTime finalPeriod,
         bool autRenew,
         string fiscalCodeProvider,
         string addressProvider,
         string mailProvider,
         string financialRemarks,
         string paymentType,
         string providerCodeBank,
         string providerAgBank,
         string providerAccountBank)
        {
            Id = id;
            CreateDate = createDate;
            UpdateDate = updateDate;
            Provider = provider;
            Remarks = remarks;
            Status = status;
            Approver = approver;
            ApproveDate = approveDate;
            TotalPrice = totalPrice;
            Requirer = requirer;
            InitialPeriod = initialPeriod;
            FinalPeriod = finalPeriod;
            AutRenew = autRenew;
            FiscalCodeProvider = fiscalCodeProvider;
            AddressProvider = addressProvider;
            MailProvider = mailProvider;
            FinancialRemarks = financialRemarks;
            PaymentType = paymentType;
            ProviderCodeBank = providerCodeBank;
            ProviderAgBank = providerAgBank;
            ProviderAccountBank = providerAccountBank;
        }
    }
}
