namespace IntraAlfa.Services.Dto
{
    public class StagesDTO
    {
        public long EntityId { get; set; }
        public string DocumentType { get; set; }
        public List<string> ApproverEmail { get; set; }
        public string Status { get; set; }
        public string Remarks { get; set; }
        public DateTime? ApproveAt { get; set; }

        public StagesDTO()
        { }

        public StagesDTO(long entityId,
        string documentType,
        List<string> approverEmail,
        string status,
        string remarks,
        DateTime approveAt
)
        {
            EntityId = entityId;
            DocumentType = documentType;
            ApproverEmail = approverEmail;
            Status = status;
            Remarks = remarks;
            ApproveAt = approveAt;

        }
    }
}