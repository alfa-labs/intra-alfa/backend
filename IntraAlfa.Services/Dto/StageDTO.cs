namespace IntraAlfa.Services.Dto
{
    public class StageDTO
    {
        public long Id { get; set; }
        public long EntityId { get; set; }
        public string DocumentType { get; set; }
        public string ApproverEmail { get; set; }
        public string Status { get; set; }
        public string Remarks { get; set; }
        public DateTime? ApproveAt { get; set; }

        public StageDTO()
        { }

        public StageDTO(long entityId,
        string documentType,
        string approverEmail,
        string status,
        string remarks,
        DateTime approveAt
)
        {
            EntityId = entityId;
            DocumentType = documentType;
            ApproverEmail = approverEmail;
            Status = status;
            Remarks = remarks;
            ApproveAt = approveAt;

        }
    }
}