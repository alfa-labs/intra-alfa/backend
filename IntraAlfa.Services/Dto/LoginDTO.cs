using System.Text.Json.Serialization;

namespace IntraAlfa.Services.Dto
{
    public class LoginDTO
    {
        public string Login { get; set; }

        public string Password { get; set; }

        public LoginDTO()
        { }

        public LoginDTO(string login, string password)
        {
            Login = login;
            Password = password;
        }
    }
}