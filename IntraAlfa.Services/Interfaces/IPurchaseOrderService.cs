﻿
using IntraAlfa.Core.Structs;
using IntraAlfa.Services.Dto;

namespace IntraAlfa.Services.Interfaces
{
    public interface IPurchaseOrderService
    {
        Task<Optional<PurchaseOrderDto>> CreateAsync(PurchaseOrderDto purchaseOrderDTO);
        Task<Optional<PurchaseOrderDto>> UpdateAsync(long id, PurchaseOrderDto purchaseOrderDTO);
        Task RemoveAsync(long id);
        Task<Optional<PurchaseOrderDto>> GetAsync(long id);
        Task<Optional<IList<PurchaseOrderDto>>> GetAllAsync();
    }
}
