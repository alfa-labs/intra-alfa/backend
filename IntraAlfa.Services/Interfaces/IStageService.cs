using IntraAlfa.Core.Structs;
using IntraAlfa.Services.Dto;

namespace IntraAlfa.Services.Interfaces
{
    public interface IStageService
    {
        Task<Optional<List<StageDTO>>> CreateAsync(StagesDTO stageDTO);
        Task RemoveAsync(long id);
        Task<Optional<StageDTO>> GetAsync(long id);
        Task<Optional<StageDTO>> Approve(long id, StageDTO stageDTO);
        Task<Optional<StageDTO>> Reject(long id, StageDTO stageDTO);
        Task<Optional<IList<StageDTO>>> GetAllByEmailAsync(string mail);
    }
}