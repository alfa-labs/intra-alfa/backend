﻿namespace IntraAlfa.Domain.Entities
{
    public class PurchaseOrder : Base
    {
        public DateTime CreateDate { get; private set; }
        public DateTime UpdateDate { get; private set; }
        public DateTime? ApproveDate { get; private set; }
        public string? Provider { get; private set; }
        public string Remarks { get; private set; }
        public string Status { get; private set; }
        public string ItemDescription { get; private set; }
        public string? Approver { get; private set; }
        public decimal TotalPrice { get; private set; }
        public string Requirer { get; set; }
        public DateTime? InitialPeriod { get; set; }
        public DateTime? FinalPeriod { get; set; }
        public bool? AutRenew { get; set; }
        public string? FiscalCodeProvider { get; set; }
        public string? AddressProvider { get; set; }
        public string? MailProvider { get; set; }
        public string? FinancialRemarks { get; set; }
        public string? PaymentType { get; set; }
        public string? ProviderCodeBank { get; set; }
        public string? ProviderAgBank { get; set; }
        public string? ProviderAccountBank { get; set; }

        public PurchaseOrder() { }

        public PurchaseOrder(
            DateTime createDate,
            DateTime updateDate,
            string provider,
            string remarks,
            DateTime approveDate,
            string status,
            string approver,
            decimal totalPrice)
        {
            CreateDate = createDate;
            UpdateDate = updateDate;
            Provider = provider;
            Remarks = remarks;
            ApproveDate = approveDate;
            Status = status;
            Approver = approver;
            TotalPrice = totalPrice;
        }

        public void SetCreateDate(DateTime createDate)
        {
            CreateDate = createDate;
        }

        public void SetUpdateDate(DateTime updateDate)
        {
            UpdateDate = updateDate;
        }

        public void SetProvider(string provider)
        {
            Provider = provider;
        }
        public void SetRemarks(string remarks)
        {
            Remarks = remarks;
        }

        public void SetApproveDate(DateTime approveDate)
        {
            ApproveDate = approveDate;
        }

        public void SetStatus(string status)
        {
            Status = status;
        }
        public void SetApprover(string approver)
        {
            Approver = approver;
        }

        public void SetTotalPrice(decimal totalPrice)
        {
            TotalPrice = totalPrice;
        }

        public void SetRequirer(string requirer)
        {
            Requirer = requirer;
        }
        public void SetInitialPeriod(DateTime initialPeriod)
        {
            InitialPeriod = initialPeriod;
        }

        public void SetFinalPeriod(DateTime finalPeriod)
        {
            FinalPeriod = finalPeriod;
        }

        public void SetAutRenew(bool autRenew)
        {
            AutRenew = autRenew;
        }
        public void SetFiscalCodeProvider(string fiscalCodeProvider)
        {
            FiscalCodeProvider = fiscalCodeProvider;
        }

        public void SetAddressProvider(string addressProvider)
        {
            AddressProvider = addressProvider;
        }

        public void SetMailProvider(string mailProvider)
        {
            MailProvider = mailProvider;
        }

        public void SetFinancialRemarks(string financialRemarks)
        {
            FinancialRemarks = financialRemarks;
        }
        public void SetPaymentType(string paymentType)
        {
            PaymentType = paymentType;
        }

        public void SetProviderCodeBank(string providerCodeBank)
        {
            ProviderCodeBank = providerCodeBank;
        }

        public void SetProviderAgBank(string providerAgBank)
        {
            ProviderAgBank = providerAgBank;
        }

        public void SetProviderAccountBank(string providerAccountBank)
        {
            ProviderAccountBank = providerAccountBank;
        }
    }
}
