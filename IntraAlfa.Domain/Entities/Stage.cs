using System.Collections.Generic;

namespace IntraAlfa.Domain.Entities
{
    public class Stage : Base
    {
        //Propriedades
        public long EntityId { get; private set; }
        public string DocumentType { get; private set; }
        public string ApproverEmail { get; private set; }
        public string Status { get; private set; }
        public string Remarks { get; private set; }
        public DateTime? ApproveAt { get; private set; }

        //EF
        protected Stage() { }

        public Stage(long entityId, string documentType, string approverEmail, string status, string remarks, DateTime approveAt)
        {
            EntityId = entityId;
            DocumentType = documentType;
            ApproverEmail = approverEmail;
            Status = status;
            Remarks = remarks;
            ApproveAt = approveAt;
            _errors = new List<string>();

        }

        public void SetEntityId(long entityId) => EntityId = entityId;
        public void SetDocumentType(string documentType) => DocumentType = documentType;
        public void SetApproverEmail(string approverEmail) => ApproverEmail = approverEmail;
        public void SetStatus(string status) => Status = status;
        public void SetRemarks(string remarks) => Remarks = remarks;
        public void SetApproveAt(DateTime approveAt) => ApproveAt = approveAt;
    }
}