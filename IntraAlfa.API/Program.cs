using AutoMapper;
using Coravel;
using EscNet.IoC.Cryptography;
using IntraAlfa.API.Token;
using IntraAlfa.API.ViewModels;
using IntraAlfa.API.ViewModels.Login;
using IntraAlfa.API.ViewModels.Stage;
using IntraAlfa.API.ViewModels.User;
using IntraAlfa.Core.Communication.Handlers;
using IntraAlfa.Core.Communication.Mediator;
using IntraAlfa.Core.Communication.Mediator.Interfaces;
using IntraAlfa.Core.Communication.Messages.Notifications;
using IntraAlfa.Domain.Entities;
using IntraAlfa.Infra.Context;
using IntraAlfa.Infra.Interfaces;
using IntraAlfa.Infra.Repositories;
using IntraAlfa.Services.Dto;
using IntraAlfa.Services.Interfaces;
using IntraAlfa.Services.Services;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;

using System.Text;
using TTRider.Aws.Beanstalk.Configuration;

var builder = WebApplication.CreateBuilder(args);

var CONN_STRING = builder.Configuration.GetConnectionString("DefaultConnection");

// Add services to the container.

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo
    {
        Title = "Intra Alfa",
        Version = "v1",
        Description = "Intranet AlfaERP.",
        Contact = new OpenApiContact
        {
            Name = "Alfa Tecnologia e Inovacao",
            Email = "contato@alfaerp.com.br",
            Url = new Uri("https://www.alfaerp.com.br")
        },
    });
});

#region AutoMapper

var autoMapperConfig = new MapperConfiguration(cfg =>
{
    cfg.CreateMap<StageViewModel, StagesDTO>().ReverseMap();
    cfg.CreateMap<StageApproveOrRejectViewModel, StageDTO>().ReverseMap(); 
    cfg.CreateMap<Stage, StagesDTO>().ReverseMap();
    cfg.CreateMap<Stage, StageDTO>().ReverseMap();
    cfg.CreateMap<StagesDTO, Stage>().ReverseMap();
    cfg.CreateMap<UserDTO, User>().ReverseMap();
    cfg.CreateMap<User, UserDTO>().ReverseMap();
    cfg.CreateMap<User, ApproverDTO>().ReverseMap();
    cfg.CreateMap<CreateUserViewModel, UserDTO>().ReverseMap();
    cfg.CreateMap<UpdateUserViewModel, UserDTO>().ReverseMap();
    cfg.CreateMap<PurchaseOrderDto, PurchaseOrder>().ReverseMap();
    cfg.CreateMap<PurchaseOrder, PurchaseOrderDto>().ReverseMap();
    cfg.CreateMap<PurchaseOrderViewModel, PurchaseOrderDto>().ReverseMap();
    cfg.CreateMap<LoginViewModel, LoginDTO>().ReverseMap();
});

builder.Services.AddSingleton(autoMapperConfig.CreateMapper());

#endregion

#region DataBase
builder.Services.AddDbContext<PurchaseOrderContext>(options => options.UseMySql(CONN_STRING, ServerVersion.AutoDetect(CONN_STRING)));
builder.Services.AddDbContext<UserContext>(options => options.UseMySql(CONN_STRING, ServerVersion.AutoDetect(CONN_STRING)));
builder.Services.AddDbContext<StageContext>(options => options.UseMySql(CONN_STRING, ServerVersion.AutoDetect(CONN_STRING)));
#endregion

#region Repositories
builder.Services.AddScoped<IPurchaseOrderRepository, PurchaseOrderRepository>();
builder.Services.AddScoped<IUserRepository, UserRepository>();
builder.Services.AddScoped<IStageRepository, StageRepository>();
#endregion

#region Services
builder.Services.AddRijndaelCryptography(builder.Configuration["Cryptography"]);
builder.Services.AddScoped<IPurchaseOrderService, PurchaseOrderService>();
builder.Services.AddScoped<ITokenService, TokenService>();
builder.Services.AddScoped<IUserService, UserService>();
builder.Services.AddScoped<IStageService, StageService>();
#endregion

#region CoravalMail
builder.Services.AddMailer(builder.Configuration);
#endregion

#region Mediator
builder.Services.AddMediatR(typeof(Program));
builder.Services.AddScoped<INotificationHandler<DomainNotification>, DomainNotificationHandler>();
builder.Services.AddScoped<IMediatorHandler, MediatorHandler>();
#endregion

#region Cors
var MyAllowSpecificOrigins = "_myAllowSpecificOrigins";
builder.Services.AddCors(options =>
{
    options.AddPolicy(name: MyAllowSpecificOrigins,
        builder =>
        {
            builder.AllowAnyOrigin()
            .AllowAnyMethod()
            .AllowAnyHeader();
        });
});
#endregion

#region Jwt

var secretKey = builder.Configuration["Jwt:Key"];

builder.Services.AddAuthentication(x =>
{
    x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
})
.AddJwtBearer(x =>
{
    x.RequireHttpsMetadata = false;
    x.SaveToken = true;
    x.TokenValidationParameters = new TokenValidationParameters
    {
        ValidateIssuerSigningKey = true,
        IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(secretKey)),
        ValidateIssuer = false,
        ValidateAudience = false
    };
});

#endregion

#region
builder.Configuration.AddBeanstalkParameters();
#endregion


var app = builder.Build();
IConfiguration configuration = app.Configuration;

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseCors(MyAllowSpecificOrigins);

//app.UseHttpsRedirection();

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();
