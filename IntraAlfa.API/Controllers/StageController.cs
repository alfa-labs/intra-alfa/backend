﻿using AutoMapper;
using IntraAlfa.API.ViewModels;
using IntraAlfa.API.ViewModels.Stage;
using IntraAlfa.Core.Communication.Messages.Notifications;
using IntraAlfa.Services.Dto;
using IntraAlfa.Services.Interfaces;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace IntraAlfa.API.Controllers
{
    [Route("/api/v1/stage")]
    [ApiController]
    public class StageController : BaseCustomController
    {
        private readonly IMapper _mapper;
        private readonly IStageService _stageService;

        public StageController(
            IMapper mapper,
            IStageService stageService,
            INotificationHandler<DomainNotification> domainNotificationHandler) : base(domainNotificationHandler)
        {
            _mapper = mapper;
            _stageService = stageService;
        }

        [HttpPost("")]
        [Authorize]
        public async Task<ActionResult> CreateAsync([FromBody] StageViewModel stageViewModel)
        {
            var stageDTO = _mapper.Map<StagesDTO>(stageViewModel);
            var stageCreated = await _stageService.CreateAsync(stageDTO);

            if (HasNotifications())
                return Result();

            return Ok(new ResultViewModel
            {
                Message = "Estágio de Aprovação criado com sucesso!",
                Success = true,
                Data = stageCreated.Value
            });
        }

        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult> GetAsync(long id)
        {
            var stage = await _stageService.GetAsync(id);

            if (HasNotifications())
                return Result();

            return Ok(new ResultViewModel
            {
                Message = "Estágio de Aprovação encontrato com sucesso!",
                Success = true,
                Data = stage.Value
            });
        }

        [HttpGet("get-by-mail")]
        [Authorize]
        public async Task<IActionResult> GetAllByEmailAsync([FromQuery(Name = "mail")] string mail)
        {
            var allStages = await _stageService.GetAllByEmailAsync(mail);

            if (HasNotifications())
                return Result();

            return Ok(new ResultViewModel
            {
                Message = "Estágios de Aprovação encontrados com sucesso!",
                Success = true,
                Data = allStages.Value
            });
        }

        [HttpPut("approve/{id}")]
        [Authorize]
        public async Task<ActionResult> Approve(long id, [FromBody] StageApproveOrRejectViewModel stageViewModel)
        {
            var stageViewModelDTO = _mapper.Map<StageDTO>(stageViewModel);
            var stageViewModelUpdated = await _stageService.Approve(id, stageViewModelDTO);

            if (HasNotifications())
                return Result();

            return Ok(new ResultViewModel
            {
                Message = "Estágio de Aprovação aprovado com sucesso!",
                Success = true,
                Data = stageViewModelUpdated.Value
            });
        }

        [HttpPut("reject/{id}")]
        [Authorize]
        public async Task<ActionResult> Reject(long id, [FromBody] StageApproveOrRejectViewModel stageViewModel)
        {
            var stageViewModelDTO = _mapper.Map<StageDTO>(stageViewModel);
            var stageViewModelUpdated = await _stageService.Reject(id, stageViewModelDTO);

            if (HasNotifications())
                return Result();

            return Ok(new ResultViewModel
            {
                Message = "Estágio de Aprovação rejeitado com sucesso!",
                Success = true,
                Data = stageViewModelUpdated.Value
            });
        }

        [HttpDelete("{id}")]
        [Authorize]
        public async Task<ActionResult> DeleteAsync(long id)
        {
            await _stageService.RemoveAsync(id);

            if (HasNotifications())
                return Result();

            return Ok(new ResultViewModel
            {
                Message = "Estágio de Aprovação removido com sucesso!",
                Success = true,
                Data = null
            });
        }
    }
}
