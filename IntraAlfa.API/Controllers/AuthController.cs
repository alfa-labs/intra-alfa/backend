﻿using AutoMapper;
using IntraAlfa.API.Token;
using IntraAlfa.API.Utilities;
using IntraAlfa.API.ViewModels;
using IntraAlfa.API.ViewModels.Login;
using IntraAlfa.Core.Communication.Messages.Notifications;
using IntraAlfa.Services.Dto;
using IntraAlfa.Services.Interfaces;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace IntraAlfa.API.Controllers
{
    [ApiController]
    public class AuthController : BaseCustomController
    {
        private readonly IConfiguration _configuration;
        private readonly ITokenService _tokenService;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public AuthController(
            IConfiguration configuration,
            ITokenService tokenService,
            IUserService userService,
            IMapper mapper,
            INotificationHandler<DomainNotification> domainNotificationHandler)
            : base(domainNotificationHandler)
        {
            _configuration = configuration;
            _tokenService = tokenService;
            _userService = userService;
            _mapper = mapper;
        }

        [HttpPost]
        [Route("/api/v1/auth/login")]
        public async Task<IActionResult> Login([FromBody] LoginViewModel loginViewModel)
        {
            var login = _mapper.Map<LoginDTO>(loginViewModel);
            var isValidLogin = await _userService.GetByEmailAndPasswordAsync(login);

            if ((bool)isValidLogin)
                return Ok(new ResultViewModel
                {
                    Message = "Usuário autenticado com sucesso!",
                    Success = true,
                    Data = new
                    {
                        Token = _tokenService.GenerateToken(loginViewModel.Login),
                        TokenExpires = DateTime.UtcNow.AddHours(int.Parse(_configuration["Jwt:HoursToExpire"]))
                    }
                });
            else
                return StatusCode(401, Responses.UnauthorizedErrorMessage());
        }
    }
}
