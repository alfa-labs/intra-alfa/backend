﻿using IntraAlfa.API.ViewModels;
using IntraAlfa.Core.Communication.Handlers;
using IntraAlfa.Core.Communication.Messages.Notifications;
using IntraAlfa.Core.Enum;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace IntraAlfa.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BaseCustomController : ControllerBase
    {
        private readonly DomainNotificationHandler _domainNotificationHandler;

        protected BaseCustomController(
           INotificationHandler<DomainNotification> domainNotificationHandler)
        {
            _domainNotificationHandler = domainNotificationHandler as DomainNotificationHandler;
        }

        protected bool HasNotifications()
           => _domainNotificationHandler.HasNotifications();

        protected ObjectResult Created(dynamic responseObject)
            => StatusCode(201, responseObject);

        protected ObjectResult Result()
        {
            var notification = _domainNotificationHandler
                .Notifications
                .FirstOrDefault();

            return StatusCode(GetStatusCodeByNotificationType(notification.Type),
                new ResultViewModel
                {
                    Message = notification.Message,
                    Success = false,
                    Data = new { }
                });
        }

        private int GetStatusCodeByNotificationType(DomainNotificationType errorType)
        {
            return errorType switch
            {
                //Conflict
                DomainNotificationType.RecordAlreadyExists
                    => 409,

                //Unprocessable Entity
                DomainNotificationType.RecordInvalid
                    => 422,

                //Not Found
                DomainNotificationType.RecordNotFound
                    => 404,

                (_) => 500,
            };
        }
    }
}
