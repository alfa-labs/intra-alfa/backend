﻿using AutoMapper;
using IntraAlfa.API.ViewModels;
using IntraAlfa.Core.Communication.Messages.Notifications;
using IntraAlfa.Services.Dto;
using IntraAlfa.Services.Interfaces;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace IntraAlfa.API.Controllers
{
    [Route("/api/v1/purchaseOrder")]
    [ApiController]
    public class PurchaseOrderController : BaseCustomController
    {
        private readonly IMapper _mapper;
        private readonly IPurchaseOrderService _purchaseOrderService;

        public PurchaseOrderController(
            IMapper mapper,
            IPurchaseOrderService purchaseOrderService,
            INotificationHandler<DomainNotification> domainNotificationHandler) : base(domainNotificationHandler)
        {
            _mapper = mapper;
            _purchaseOrderService = purchaseOrderService;
        }

        [HttpPost("")]
        [Authorize]
        public async Task<ActionResult> CreateAsync([FromBody] PurchaseOrderViewModel purchaseOrderViewModel)
        {
            var purchaseOrderDTO = _mapper.Map<PurchaseOrderDto>(purchaseOrderViewModel);
            var purchaseOrderCreated = await _purchaseOrderService.CreateAsync(purchaseOrderDTO);

            if (HasNotifications())
                return Result();

            return Ok(new ResultViewModel
            {
                Message = "Pedido criado com sucesso!",
                Success = true,
                Data = purchaseOrderCreated.Value
            });
        }

        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult> GetAsync(long id)
        {
            var purchaseOrder = await _purchaseOrderService.GetAsync(id);

            if (HasNotifications())
                return Result();

            return Ok(new ResultViewModel
            {
                Message = "Pedido encontrato com sucesso!",
                Success = true,
                Data = purchaseOrder.Value
            });
        }

        [HttpGet]
        [Route("get-all")]
        [Authorize]
        public async Task<IActionResult> GetAsync()
        {
            var allUsers = await _purchaseOrderService.GetAllAsync();

            if (HasNotifications())
                return Result();

            return Ok(new ResultViewModel
            {
                Message = "Pedidos encontrados com sucesso!",
                Success = true,
                Data = allUsers.Value
            });
        }

        [HttpPut("{id}")]
        [Authorize]
        public async Task<ActionResult> UpdateAsync(long id, [FromBody] PurchaseOrderViewModel purchaseOrderViewModel)
        {
            var purchaseOrderDTO = _mapper.Map<PurchaseOrderDto>(purchaseOrderViewModel);
            var purchaseOrderUpdated = await _purchaseOrderService.UpdateAsync(id, purchaseOrderDTO);

            if (HasNotifications())
                return Result();

            return Ok(new ResultViewModel
            {
                Message = "Pedido atualizado com sucesso!",
                Success = true,
                Data = purchaseOrderUpdated.Value
            });
        }

        [HttpDelete("{id}")]
        [Authorize]
        public async Task<ActionResult> DeleteAsync(long id)
        {
            await _purchaseOrderService.RemoveAsync(id);

            if (HasNotifications())
                return Result();

            return Ok(new ResultViewModel
            {
                Message = "Pedido removido com sucesso!",
                Success = true,
                Data = null
            });
        }
    }
}
