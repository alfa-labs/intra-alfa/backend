﻿using System.ComponentModel.DataAnnotations;

namespace IntraAlfa.API.ViewModels.Login
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "O login não pode vazio.")]
        public string Login { get; set; }

        [Required(ErrorMessage = "A senha não pode vazio.")]
        public string Password { get; set; }
    }
}
