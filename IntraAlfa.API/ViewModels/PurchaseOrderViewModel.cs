﻿namespace IntraAlfa.API.ViewModels
{
    public class PurchaseOrderViewModel
    {
        public long Id { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public string Requirer { get; set; }
        public string ItemDescription { get; set; }
        public DateTime InitialPeriod { get; set; }
        public DateTime FinalPeriod { get; set; }
        public bool AutRenew { get; set; }
        public string FiscalCodeProvider { get; set; }
        public string AddressProvider { get; set; }
        public string MailProvider { get; set; }
        public string FinancialRemarks { get; set; }
        public string Provider { get; set; }
        public string Remarks { get; set; }
        public string PaymentType { get; set; }
        public string ProviderCodeBank { get; set; }
        public string ProviderAgBank { get; set; }
        public string ProviderAccountBank { get; set; }
        public decimal TotalPrice { get; set; }
        public string Status { get; set; }
        public string Approver { get; set; }
        public DateTime ApproveDate { get; set; }
    }
}
