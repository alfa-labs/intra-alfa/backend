﻿using System.ComponentModel.DataAnnotations;

namespace IntraAlfa.API.ViewModels.Stage
{
    public class StageViewModel
    {
        [Required(ErrorMessage = "O EntityId não pode ser vazio.")]
        public long EntityId { get; set; }

        [Required(ErrorMessage = "O DocumentType não pode ser vazio.")]
        public string DocumentType { get; set; }

        [Required(ErrorMessage = "O email não pode ser vazio.")]
        public List<string> ApproverEmail { get; set; }

        [Required(ErrorMessage = "A Status não pode ser vazia.")]
        public string Status { get; set; }

        public string Remarks { get; set; }

        public DateTime? ApproveAt { get; set; }
    }
}
