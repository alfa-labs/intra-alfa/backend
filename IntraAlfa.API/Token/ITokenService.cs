﻿namespace IntraAlfa.API.Token
{
    public interface ITokenService
    {
        string GenerateToken(string login);
    }
}
