﻿namespace IntraAlfa.Core.Enum
{
    public enum DomainNotificationType
    {
        RecordAlreadyExists,
        RecordInvalid,
        RecordNotFound,
    }
}
