﻿namespace IntraAlfa.Core.Validations.Message
{
    public static class ErrorMessages
    {
        public const string RecordAlreadyExists
            = "Já existe um registro cadastrado com os dados informados.";

        public const string RecordNotFound
            = "Não existe nenhum registro com o id informado.";

        public static string RecordInvalid(string errors)
            => "Os campos informados para o registro estão inválidos" + errors;
    }
}
