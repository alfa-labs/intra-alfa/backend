﻿using IntraAlfa.Core.Communication.Messages.Notifications;

namespace IntraAlfa.Core.Communication.Mediator.Interfaces
{
    public interface IMediatorHandler
    {
        Task PublishDomainNotificationAsync<T>(T appNotification)
            where T : DomainNotification;
    }
}
